import _ from 'lodash';
import './style.scss';

fetch('https://baconipsum.com/api/?type=all-meat&sentences=1&start-with-lorem=1')
.then(response => response.json())
.then(response => {
  document.getElementById('lorem-ipsum').innerHTML = response[0];
  document.getElementById('lorem-ipsum2').innerHTML = response[0];
  document.getElementById('lorem-ipsum3').innerHTML = response[0];
  document.getElementById('lorem-ipsum4').innerHTML = response[0];
  document.getElementById('lorem-ipsum5').innerHTML = response[0];
  document.getElementById('lorem-ipsum6').innerHTML = response[0];
})
.catch(error => console.error(error));