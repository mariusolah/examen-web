const path = require('path');

module.exports = {
  entry: './src/index.js',
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  },
    output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
         rules: [
            {
                test: /\.s?css$/,
                use: ['style-loader', 'css-loader', 'sass-loader']          
              },
         ],
       },
};