<!doctype html>
<html>

<head>
  <title>Getting Started</title>
  <script src="https://unpkg.com/lodash@4.16.6"></script>
  <link rel="stylesheet" href="../src/style.scss">
  <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
</head>
<?php
    require('../lib/function.php');
?>
<body>
  <nav class="navbar navbar-expand-lg navbar-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <img class="image-header" src="images/logo@2x.png">
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <ul class="navbar-nav mr-auto">
          <li class="design-header pt-2 pb-2 nav-item">
            <a class="font weight-bold text-white nav-link" href="#">HOME</a>
          </li>
          <li class="design-header p-2 nav-item dropdown">
            <a class="font weight-bold text-white nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              PAGES
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">FIRST</a>
              <a class="dropdown-item" href="#">SECOND</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">ALL PAGES</a>
            </div>
          </li>
          <li class="design-header p-2 nav-item dropdown">
            <a class="font weight-bold text-white nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              PORTOFOLIO
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">PORTOFOLIO 2</a>
              <a class="dropdown-item" href="#">PORTOFOLIO 3</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">ALL PORTOFOLIOS</a>
            </div>
          </li>
          <li class="design-header p-2 nav-item dropdown">
            <a class="font weight-bold text-white nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              BLOG
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">BLOG LEFT</a>
              <a class="dropdown-item" href="#">BLOG RIGHT</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">ALL BLOGS</a>
            </div>
          </li>
        </ul>
      </form>
    </div>
  </nav>

  <div class="container">
    <div class="mt-5 mb-5 row">
      <div class="text-center col">
        <h4>WELCOME TO  <span class="welcome-to-span">AT RESTAURANT</span></h4>
        <p id="lorem-ipsum"></p>
      </div>
    </div>
    <div class="row">
      <div class="text-center col-md col-sm-12 col-12">
        <img class="mb-2" src="images/icons8-dining-room-50.png">
        <p id="lorem-ipsum2"></p>
      </div>
      <div class="text-center col-md col-sm-12 col-12">
          <img class="mb-2" src="images/icons8-wine-glass-50.png">
        <p id="lorem-ipsum3"></p>
      </div>
      <div class="text-center col-md col-sm-12 col-12">
        <img class="mb-2" src="images/icons8-beer-50.png">
        <p id="lorem-ipsum4"></p>
      </div>
      <div class="text-center col-md col-sm-12 col-12">
          <img class="mb-2" src="images/icons8-coffee-50.png">
        <p id="lorem-ipsum5"></p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="mt-5 row">
      <div class="text-center col">
        <h4 class="mb-3">Latest work</h4>
        <p id="lorem-ipsum6"></p>
      </div>
    </div>
    <?php
$result = queryResult('SELECT * from produse');
?>
  <div class="mt-5 row">
  <?php      
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) { 
        ?>
        <div class="text-center col">
            <a  class="buton-categorie btn" data-toggle="pill" href="#<?php echo $row["categorie"] ?>"><?php echo $row["categorie"] ?></a>
        </div>
      <?php   
      }
  }  else {
      `echo "0 results`;
    }
    ?>
    </div>
    <div class="container">
    <div class="mt-5 row tab-content">
    <div id="Lactate" class="text-center col tab-pane fade">
        <img src="images/lactate.jpg">
  </div>
    <div id="Fast-food" class="text-center col tab-pane fade">
        <img src="images/fast-food.jpg">
  </div>
    <div id="Dulce" class="text-center col tab-pane fade">
        <img src="images/dulce.jpg">
  </div>
    <div id="Vegetale" class="text-center col tab-pane fade">
        <img src="images/vegetal.jpg">
    </div>
  </div>
  </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
  <script src="bundle.js"></script>
</body>

</html>